# README

Simple Panel Admin Ruby On Rails by Kamil "YamiNoKam" K.

* Ruby version :3.5.2

* Rails version : 5.1

To run this, do before(on ruby cli, folder project):

1. Install bundler
<code>gem install bundler</code>

2. Use bundler to install all needed gems
<code>bundle install</code>

3. Install rake to and make db:
<code>gem install rake</code>

4. Make migrate to create table
<code>rake db:migrate</code>

5. Run server
<code>rails s</code>

6. Log 1st created user
<code>Email: test@test.com
Password:test123</code>

7. Have fun :)!