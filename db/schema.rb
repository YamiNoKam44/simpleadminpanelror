# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170917103123) do

  create_table "addtional_emails", force: :cascade do |t|
    t.string "main_email", limit: 50
    t.string "addtonal_email", limit: 50
    t.index ["main_email"], name: "index_addtional_emails_on_main_email"
  end

  create_table "users", force: :cascade do |t|
    t.string "firstname", limit: 50
    t.string "surname", limit: 50
    t.string "email", limit: 120, null: false
    t.string "password_digest", null: false
    t.index ["email"], name: "index_users_on_email"
  end

end
