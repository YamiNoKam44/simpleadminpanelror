class CreateAddtionalEmails < ActiveRecord::Migration[5.1]
  def up
    create_table :addtional_emails do |t|
      t.string "main_email", :limit=>50
      t.string "addtonal_email", :limit=>50
    end
    add_index("addtional_emails", "main_email")
  end

  def down
    drop_table :addtional_emails
  end
end
