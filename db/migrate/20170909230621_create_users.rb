class CreateUsers < ActiveRecord::Migration[5.1]
  def up
    create_table :users do |u|
      u.string "firstname", :limit=>50
      u.string "surname", :limit=>50
      u.string "email", :limit=>120,  :null => false, unique: true
      u.string "password_digest" ,  :null => false
    end
    add_index("users", "email")
    User.create :firstname => "admin",
                :surname => "admin",
                :email => "test@test.com",
                :password =>'test123'
  end

  def down
    drop_table :users
  end
end
