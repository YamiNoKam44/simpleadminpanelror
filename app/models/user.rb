class User < ApplicationRecord
  has_secure_password

  validates :email,
           :presence => true,
            :length => {maximum: 120,:message => ': Your email is too long'},
             uniqueness: true,
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/ }
  validates :password_digest,
            :presence => true,
            :length => {minimum: 6, :message => ': Your need to have minimum 6 characters'}
end
