class AddtionalEmail < ApplicationRecord
  validates :addtonal_email,
            :presence => true,
            :length => {maximum: 50,:message => ': Your email is too long'},
            uniqueness: true,
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/ }
end
