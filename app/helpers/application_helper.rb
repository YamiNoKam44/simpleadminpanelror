module ApplicationHelper
  def error(object)
    render(:partial => 'addtional/errors', :locals => {:object => object})
  end
end
