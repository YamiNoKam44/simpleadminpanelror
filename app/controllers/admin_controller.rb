class AdminController < ApplicationController

before_action :login_check

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    @user = User.new()
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find_by(params[:email])
    if @user.update(user_params)
      flash[:notice] = 'User has been updated.'
      redirect_to('/admin/index')
    else
      render("edit")
    end
  end

  def delete
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = "User has been deleted."
    redirect_to('/admin/index')
  end

  def create_user
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "User has been created."
      redirect_to('/admin/index')
    else
      render("create")
    end
  end

  def user_params
    params.require(:user).permit(:firstname, :surname, :email, :password)
  end
end
