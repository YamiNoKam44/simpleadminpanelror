class AddtionalEmailController < ApplicationController
layout false
    def index
      @userAddEmail = AddtionalEmail.where(main_email: Base64.urlsafe_decode64(params[:id]))
    end

    def create
    @userAddEmail = AddtionalEmail.create(adParams)
  end

  def adParams
    params.require(:addtionalEmail).permit(:main_email,:addtonal_email)
  end

  def delete
    @userAddEmail = AddtionalEmail.find(params[:id])
    @userAddEmail.destroy
  end

  def create_addtional_email
    @userAddEmail = AddtionalEmail.new(adParams)
    @userAddEmail.save
  end
end
