class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

private
  def login_check
    unless session[:user]
      flash[:notice] = "You're not logged, login to continue"
      redirect_to(:controller => 'access',:action => 'login')
      return false
    else
      return true
    end
  end
end
