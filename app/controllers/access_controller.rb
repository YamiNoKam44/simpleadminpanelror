class AccessController < ApplicationController

  before_action :login_check, :except => [:login, :loging, :logout]

  def index
  end

  def login
  end

  def loging
    if params[:email].present? && params[:password].present?
      admin_search = User.where(:email => params[:email]).first
      if admin_search
        authorize = admin_search.authenticate(params[:password])
      end
    end

    if authorize
      session[:user] = authorize.email
      flash[:notice] = 'Login successful'
      redirect_to('/admin/index')
    else
      flash[:notice] = 'Error: email or password is incorrect'
      redirect_to('login')
    end
  end

  def logout
    session[:user] = nil
    flash[:notice] = "You're logged off"
    redirect_to(:action => 'login')
  end
end
